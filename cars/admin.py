from django.contrib import admin
from cars.models import CarModel,Review

class CarAdmin(admin.ModelAdmin):
     fieldsets = [
         ("Time Information",{'fields':['year']}),
         ("CAR INFORMATION", {'fields': ['brand']})
     ]

admin.site.register(CarModel,CarAdmin)

admin.site.register(Review)
