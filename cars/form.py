from django import  forms
from django.forms import  ModelForm
from cars.models import Review
# class ReviewForm(forms.Form):
#     first_name = forms.CharField(label="Fisrt Name", max_length=100)
#     last_name = forms.CharField(label="Last Name", max_length=100)
#     email = forms.EmailField(label="Email")
#     review = forms.CharField(label="review",widget=forms.Textarea())



class ReviewForm(ModelForm):
    class Meta :
      model = Review
      #fields = ["first_name","last_name","stars"]
      fields = "__all__"

      labels = {
        "first_name":"Your first name",
        "last_name": "Your last_name",
        "stars": "Your stars"
      }

      error_messages = {
         "stars" : {
           "min_value" : "min value is 1",
           "max_value": "max value is 5"
       }
      }
