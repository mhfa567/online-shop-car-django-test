from django.shortcuts import render,redirect
from  django.urls import reverse,reverse_lazy
from django.views.generic import FormView
from . import models
from .form import ReviewForm


class RentalReviewFormView (FormView):
  form_class = ReviewForm
  template_name = 'car html/rental_review.html'
  success_url = reverse_lazy("cars:thank_you")
  def form_valid(self, form):
    form.save()
    return super().form_valid(form)



def list (request):

  all_cars = models.CarModel.objects.all()
  context = {"all_cars":all_cars}

  return  render(request ,'car html/list.html',context=context)

def add (request):
  if request.POST:
    brand = request.POST["brand"]

    year = int(request.POST["year"])

    models.CarModel.objects.create(brand=brand,year=year)

    return redirect(reverse("cars:list"))

  else:
      return  render(request ,'car html/add.html')

def delete (request):
  if request.POST:
    pk = request.POST["pk"]
    try:
      models.CarModel.objects.get(pk=pk).delete()
      return redirect(reverse("cars:list"))
    except:
      return redirect(reverse("cars:list"))
  else:
     return  render(request ,'car html/delete.html')

def rental_review (request):
  if request.POST:
    form = ReviewForm(request.POST)
    if form.is_valid():
      form.save()
      return redirect(reverse("cars:thank_you"))
  else:
    form = ReviewForm()
  return render(request, 'car html/rental_review.html', context={'form':form} )

def thank_you (request):
  return render(request, 'car html/thank_you.html')



