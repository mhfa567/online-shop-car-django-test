from django.urls import path,include
from . import views
from  .views import RentalReviewFormView

app_name = 'cars'

urlpatterns = [
   path('list/',views.list,name='list'),
   path('add/',views.add,name='add'),
   path('delete/',views.delete,name='delete'),
   path('rental_review/',RentalReviewFormView.as_view(),name='rental_review'),
   path('thank_you/',views.thank_you,name='thank_you')
]